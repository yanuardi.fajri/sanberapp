import React from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  FlatList,
  TextInput
} from 'react-native';
import { Component } from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { AntDesign } from '@expo/vector-icons'; 
import { FontAwesome } from '@expo/vector-icons'; 

export default class AboutScreen extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Text style={{fontSize: 36, fontWeight: "bold"}}>Tentang Saya</Text>
                <FontAwesome name="user-circle-o" size={100} color="#CACACA" />
                
                <Text style={{fontSize: 36, fontWeight: "bold", marginBottom: 10}}>Yanuardi Fajri</Text>
                <Text style={{fontSize: 16, color: "#3EC6FF", margin: 10}}>React Native Developer</Text>
                <View style={styles.box}>
                    <Text style={{borderBottomWidth: 2, fontSize: 18}}>Portofolio</Text>
                    <View style={{flexDirection: 'row'}}>
                        <View style={{flex: 2}}>
                        <FontAwesome name="gitlab" size={50} color="#3EC6FF" />
                        <Text>@yanuardifajri</Text>

                        </View>
                        <View style={{flex: 2}}>
                        <AntDesign name="github" size={50} color="#3EC6FF" />
                        <Text>@yanuardifajri</Text>

                        </View>
                        

                    </View>

                </View>
                <View style={styles.box}>
                    <Text style={{borderBottomWidth: 2, fontSize: 18}}>Hubungi Saya</Text>
                    <View style={{flexDirection: 'column'}}>
                        <View style={{flex: 2, flexDirection:'row'}}>
                        <FontAwesome name="gitlab" size={50} color="#3EC6FF" />
                        <Text>@yanuardifajri</Text>

                        </View>
                        <View style={{flex: 2, flexDirection:'row'}}>
                        <AntDesign name="github" size={50} color="#3EC6FF" />
                        <Text>@yanuardifajri</Text>

                        </View>
                        

                    </View>

                </View>

            </View>

        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      flexDirection: "column",
    },
    logo: {
        //flex: 1,
        width: 375,
        height: 102,
        paddingTop: 63,
        marginBottom: 70
    },
    box: {
        marginBottom: 10,
        backgroundColor: "#EFEFEF",
        width: 359,
        width: 140,
        borderRadius: 16,
        padding: 15
    }
});
