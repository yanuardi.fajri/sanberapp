import React from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  FlatList,
  TextInput
} from 'react-native';
import { Component } from 'react';

export default class SignUp extends Component {
    render() {
        return (
            <View style={styles.container}>
                
                <Image source={require('./assets/logo.png')} style={styles.logo} />
                <Text style={styles.title}>Register</Text>
                <View>
                    <Text style={{color: '#003366', marginBottom: 5}}>Username</Text>
                    <TextInput style={styles.inputBoxBase}></TextInput>
                    <Text style={{color: '#003366', marginBottom: 5}}>Email</Text>
                    <TextInput style={styles.inputBoxBase}></TextInput>
                    <Text style={{color: '#003366', marginBottom: 5}}>Password</Text>
                    <TextInput style={styles.inputBoxBase}></TextInput>
                    <Text style={{color: '#003366', marginBottom: 5}}>Ulangi Password</Text>
                    <TextInput style={styles.inputBoxBase}></TextInput>
                </View>
                
                <TouchableOpacity style={styles.button}>
                    <Text style={{color: 'white'}}>Daftar</Text>
                </TouchableOpacity>
                <Text style={{marginVertical: 10, color: '#3EC6FF'}}>atau</Text>
                <TouchableOpacity style={[styles.button, {backgroundColor: '#3EC6FF'}]}>
                    <Text style={{color: 'white'}}>Masuk</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      flexDirection: "column",
    },
    logo: {
        //flex: 1,
        width: 375,
        height: 102,
        paddingTop: 63,
        marginBottom: 70
    },
    title: {
        //flex: 1,
        width: 88,
        height: 28,
        fontSize: 24,
        lineHeight: 28,
        color: '#003366',
        marginBottom: 40,

    },
    inputBoxBase: {
        //flex: 1,
        width: 294,
        height: 48, 
        marginBottom: 20,
        borderColor: '#003366', 
        borderWidth: 1
    },
    button: {
        borderRadius: 16,
        backgroundColor: '#003366',
        width: 140,
        height: 40,
        alignItems: "center",
        justifyContent: 'center',   

    }
    
})





